var el = document.getElementById("generate_bar");
let cookie = document.cookie
let csrfToken = cookie.substring(cookie.indexOf('=') + 1)
el.addEventListener("change", changeGraph, false);

function changeGraph() {
    $.ajax({
        type: "POST",
        url: "my-ajax-test/",
        headers: {
            'X-CSRFToken': csrfToken
        },
        data: {value: el.value},
        success: function (response) {
            const obj = JSON.parse(response);
            console.log(obj);
            Plotly.newPlot('graph_zone', obj.data, obj.layout)
        }
    });

}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}