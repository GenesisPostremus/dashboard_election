from urllib.error import URLError
import json
import pandas
from pandas.errors import ParserError

ALLOWED_EXTENSION = ["csv", "txt", "xls", "json"]


class DataPull:

    def get_data(self, url, separator=None, *args, **kwargs):
        extension = self.get_extension(url)
        if extension not in ALLOWED_EXTENSION:
            raise NotAllowedExtention("File extension not allowed")

        try:
            if extension in ["csv", "txt"]:
                return pandas.read_csv(filepath_or_buffer=url, sep=separator, *args, **kwargs)
            if "xls" in extension:
                return pandas.read_excel(url, *args, **kwargs)
            if "json" in extension:
                return pandas.DataFrame(json.load(open(url))["result"])
        except URLError as msg:
            raise UrlNotValid(msg.__str__())
        except ParserError:
            raise FileNotFoundError("File coulnd't be parsed")

    def get_extension(self, url: str):
        # String after last /
        file: str = url.split("/")[-1]
        if "." not in file:
            raise NoExtensionError("File with no extension or url don't lead to a file")
        return file.split(".")[-1]


class NoExtensionError(Exception):
    pass


class NotAllowedExtention(Exception):
    pass


class UrlNotValid(Exception):
    pass
