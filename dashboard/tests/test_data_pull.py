from unittest.mock import patch

import django.test
import pandas

from dashboard.processing.data_pull import DataPull, NoExtensionError, NotAllowedExtention, UrlNotValid


class TestDataPull(django.test.SimpleTestCase):
    data_pull = DataPull()

    @patch("pandas.read_csv", return_value=pandas.DataFrame())
    def test_get_data_return_pdandas_dataframe_if_csv(self, mock_read_csv):
        response = self.data_pull.get_data(url="https://test_data.com/home.csv")
        self.assertIsInstance(response, pandas.DataFrame)

    @patch("pandas.read_excel", return_value=pandas.DataFrame())
    def test_get_data_return_pdandas_dataframe_if_xls(self, mock_read_csv):
        response = self.data_pull.get_data(url="https://test_data.com/home.xls")
        self.assertIsInstance(response, pandas.DataFrame)

    def test_get_data_return_file_error_couldnt_be_parsed(self):
        with self.assertRaises(FileNotFoundError) as e:
            self.data_pull.get_data(url="https://twitter.com/home.csv")
        self.assertEqual("File coulnd't be parsed", str(e.exception))

    def test_get_data_return_url_not_valid_if_so(self):
        with self.assertRaises(UrlNotValid) as e:
            self.data_pull.get_data(url="http:/badurl.csv")

        self.assertEqual("<urlopen error no host given>", str(e.exception))

    def test_get_data_data_return_not_allowed_extension_if_so(self):
        with self.assertRaises(NotAllowedExtention) as e:
            self.data_pull.get_data(url="http://test_data/file.notallowed")

        self.assertEqual("File extension not allowed", str(e.exception))

    def test_get_data_return_no_file_no_extension_error_if_no_file_extension(self):
        with self.assertRaises(NoExtensionError) as e:
            self.data_pull.get_data(url="http://test_data/")

        self.assertEqual("File with no extension or url don't lead to a file", str(e.exception))

    def test_get_extension_return_csv_for_csv_file(self):
        response = self.data_pull.get_extension(url="http://test_data/file.csv")

        self.assertEqual("csv", response)

    def test_get_extension_return_txt_for_txt_file(self):
        response = self.data_pull.get_extension(url="http://test_data/file.txt")

        self.assertEqual("txt", response)

    def test_get_extension_return_xls_for_txt_file(self):
        response = self.data_pull.get_extension(url="http://test_data/file.xls")

        self.assertEqual("xls", response)

    def test_get_extension_return_no_extension_error_if_no_file_extension(self):
        with self.assertRaises(NoExtensionError) as e:
            self.data_pull.get_extension(url="http://test_data/file")
        self.assertEqual("File with no extension or url don't lead to a file", str(e.exception))
