import django.test
from django.urls import reverse


class TestView(django.test.SimpleTestCase):
    def test_index_view_return_template(self):
        response = self.client.get(reverse("index"))

        self.assertTemplateUsed(response, "index.html")
