# import geopandas
import plotly.express as px
import plotly.graph_objects as go
import plotly
# import pyproj
import pandas
from django.shortcuts import render
# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_GET

from dashboard.processing.data_pull import DataPull
from django.http import HttpResponse
import json


@require_GET
def index(request):
    generate_bar_1 = generate_bar()
    # generate_funnel_1 = generate_funnel()

    return render(request, "index.html", context={
        'popularity': popularity_president().to_html(),
        'generate_bar_1': generate_bar_1.values.tolist(),
        # "region_dataframe": generate_map_region()
        'presidentielle2022': get_data_sondage_presidentielle2022_parmois(request).to_html(),
        'monthpresidentielle2022': getmoissondage2022(request),
        'sondage_line': sondage_line_2022().to_html()
    })


def get_data_sondage_presidentielle2022_parmois(request):
    import calendar
    mois = getmoissondage2022(request)
    df = DataPull().get_data(
        url="https://raw.githubusercontent.com/nsppolls/nsppolls/master/presidentielle.csv",
    )
    df = df[df['debut_enquete'].str.contains("^"+mois)]
    df["intentions"] = df["intentions"].astype(int)
    df1 = pandas.DataFrame(df.groupby(by="candidat").sum())
    df1["intentions"] = df1["intentions"].round(2)
    df1 = df1.sort_values(by="intentions")
    df1.reset_index(inplace=True)
    moistab = mois.split('-')
    plot_pie = go.Figure(go.Pie(
        values=df1["intentions"],
        labels=df1["candidat"],
        hovertemplate="%{label}: <br> Popularité: %{percent} <extra></extra>"
    ), layout=go.Layout(
        title=go.layout.Title(text="Pourcentage des voix par candidats en "+calendar.month_name[int(moistab[1])]+" "+moistab[0])
    ))
    return plot_pie


def generate_pie():
    df = px.data.gapminder().query("year == 2007").query("continent == 'Europe'")
    df.loc[df['pop'] < 2.e6, 'country'] = 'Other countries'  # Represent only large countries
    plot_pie = px.pie(df, values='pop', names='country', title='Population of European continent')
    return plot_pie


def popularity_president():
    data = DataPull().get_data(
        url="https://raw.githubusercontent.com/pollsposition/data/main/sondages/popularite.csv"
    ).query("sondage == 'Ifop'")

    fig = px.line(data, x='Unnamed: 0', y='approve_pr', color='president')
    fig.update_layout(
        xaxis_title="Trimestre de présidence",
        yaxis_title="Pourcentage popularité",
        title="Popularité des présidents de la Vème République"
    )
    return fig


def sondage_line_2022():
    data = DataPull().get_data(
        url="https://raw.githubusercontent.com/nsppolls/nsppolls/master/presidentielle.csv"
    ).query("nom_institut == 'Ifop'")
    data = data[data["candidat"].str.contains("Anne Hidalgo|Yannick Jadot|Marine Le Pen|Valérie Pécresse|Eric Zemmour|Emmanuel Macron|Jean-Luc Mélenchon")]
    data['fin_enquete'] = pandas.to_datetime(data['fin_enquete'])
    data = data.sort_values(by='fin_enquete')
    data = data.drop_duplicates(['candidat', 'fin_enquete'])
    data = data.loc[(data['fin_enquete'] >= '2021-05-01')]
    fig = px.line(data, x='fin_enquete', y='intentions', color='candidat')
    fig.update_layout(
        xaxis_title="Date",
        yaxis_title="Intentions de votes",
        title="Sondage au cours du temps, présidentielle 2022"
    )
    return fig


def generate_bar():
    data_tour_1 = DataPull().get_data(
        url="https://www.data.gouv.fr/s/resources/election-presidentielle-des-23-avril-et-7-mai-2017-resultats-definitifs-du-1er-tour-1/20170427-100131/Presidentielle_2017_Resultats_Tour_1_c.xls",
        sheet_name="Régions Tour 1"
    )
    data_tour_1.insert(0, 'NEW_ID', range(0, 0 + len(data_tour_1)))
    data_tour_1 = data_tour_1.rename(columns={"Unnamed: 1": "region"})
    data_tour_1 = data_tour_1.drop([0, 1, 2, 3, 4])
    data_tour_1 = data_tour_1[["NEW_ID", "region"]]
    return data_tour_1


@csrf_exempt
def generate_funnel(request):
    data_tour_2 = DataPull().get_data(
        url="https://www.data.gouv.fr/s/resources/election-presidentielle-des-23-avril-et-7-mai-2017-resultats-definitifs-du-1er-tour-1/20170427-100131/Presidentielle_2017_Resultats_Tour_1_c.xls",
        sheet_name="Régions Tour 1"
    )

    data_tour_2 = data_tour_2.rename(
        columns={"Unnamed: 2": "inscrits", "Unnamed: 5": "votants", "Unnamed: 3": "abstentions",
                 "Unnamed: 7": "blancs"})
    data_tour_2 = data_tour_2.iloc[int(request.POST['value'])]

    fig = go.Figure(go.Funnel(
        y=["Inscrits", "Votans", "Abstentions", "Blancs"],
        x=[data_tour_2.inscrits, data_tour_2.votants, data_tour_2.abstentions, data_tour_2.blancs],
        textinfo="value+percent initial",
        opacity=0.65, marker={"color": ["deepskyblue", "lightsalmon", "tan", "teal"],
                              "line": {"width": [4, 2, 2, 3, 1, 1], "color": ["wheat", "wheat", "blue", "wheat"]}},
        connector={"line": {"color": "royalblue", "dash": "dot", "width": 3}})
    )
    return HttpResponse(fig.to_json())



def generate_map_region():
    return "Hello World"
    # data = DataPull().get_data(
    #     url="https://www.data.gouv.fr/s/resources/election-presidentielle-des-23-avril-et-7-mai-2017-resultats-definitifs-du-2nd-tour/20170511-092258/Presidentielle_2017_Resultats_Tour_2_c.xls",
    #     sheet_name="Régions Tour 2", skiprows=[0, 1, 2, 3],
    #     usecols=["Code de la région", "Libellé de la région", "% Abs/Ins"]
    # )
    # data = data.replace(["Auvergne et Rhône-Alpes"], ["Auvergne-Rhône-Alpes"])
    # region_dataframe = geopandas.read_file(
    #     "https://france-geojson.gregoiredavid.fr/repo/regions.geojson")
    # region_dataframe.to_crs(pyproj.CRS.from_epsg(4326), inplace=True)

    # df_merged = region_dataframe.merge(data, left_on="nom", right_on="Libellé de la région")[
    #     ["code", "nom", "geometry", "% Abs/Ins"]]

    # fig = px.choropleth(df_merged, geojson=df_merged["geometry"], locations=df_merged.index, color="% Abs/Ins",
    #                     color_continuous_scale="Viridis", hover_data=["nom"])
    # fig.update_geos(fitbounds="locations", visible=True)
    # fig.update_layout(
    #     title_text='Map'
    # )
    # fig.update(layout=dict(title=dict(x=0.5)))
    # fig.update_layout(
    #     margin={"r": 0, "t": 30, "l": 10, "b": 10},
    #     coloraxis_colorbar={
    #         'title': "% Abs/Ins"})
    # names = [data["Libellé de la région"], region_dataframe["nom"]]
    # return fig.to_html()})


def getmoissondage2022(request):
    import datetime
    import locale
    locale.setlocale(locale.LC_ALL, 'fr_FR')
    now = datetime.datetime.now()
    if now.month >= 10:
        now = ("%s" % now.year) + "-" + ("%s" % now.month)
    else:
        now = ("%s" % now.year) + "-" + ("0%s" % now.month)
    mois = request.GET.get('moissondage2022', now)
    return mois